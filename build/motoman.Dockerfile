FROM osrf/ros:indigo-desktop-full

RUN apt update && curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add - \
    apt-get install -y nano && \
    apt-get install -y tmux && \
    apt-get install -y net-tools && \
    apt-get install -y ros-indigo-moveit

RUN useradd -m -s /bin/bash -N -u 1000 motoman && \
    echo "motoman ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers && \
    chmod 0440 /etc/sudoers && \
    chmod g+w /etc/passwd 

USER motoman

WORKDIR home/motoman/app

COPY --chown=motoman app/ .

RUN chmod 774 ~/app/motoman_ws/

RUN /bin/bash -c "source /opt/ros/indigo/setup.bash;"
