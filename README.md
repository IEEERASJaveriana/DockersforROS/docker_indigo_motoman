# docker_indigo_motoman

## Table of contents
* [Description](#description)
* [Technologies](#technologies)
* [Setup](#setup)
* [Licence](#licence)

## Description
This proyect was made for the ieee chapter "RAS Javeriana", it is a docker with all the necesary packages to control motoman SDA10D, load motoman SDA10D in rviz and in gazebo. 

## Technologies
Project created with:
* Ubuntu: 14.04
* Docker: 23.0.6

## Setup
To run this project in ubuntu 20.04, you need to install these dependencies and run this commands to use docker:
```
$ sudo apt update
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
$ sudo apt install docker-ce
$ sudo usermod -aG docker ${USER}
```

If this is your first time running this docker, you need to run this commands:
```
$ git clone https://gitlab.com/IEEERASJaveriana/DockersforROS/docker_indigo_motoman.git
$ cd docker_indigo_motoman
$ ./run_emptyVolume.sh
$ source init.bash  #If you are asked to accept a licence write "yes"
```

If you have already runned this docker before use this commands:
```
$ cd docker_indigo_motoman
$ ./run.sh
$ source init.bash  #If you are asked to accept a licence write "yes"
```

## Licence
ros_inidigo_motoman is available under the BSD-3-Clause license. See the LICENSE file for more details.