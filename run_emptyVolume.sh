#!/bin/bash

docker build -f build/motoman.Dockerfile -t motomanimage .

docker run --rm -it --name motoman\
	--env="DISPLAY" \
    	--env="QT_X11_NO_MITSHM=1" \
    	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--network host \
	-v motomanVol:/app \
	motomanimage:latest
