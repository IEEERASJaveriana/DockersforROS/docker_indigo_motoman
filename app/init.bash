#!/bin/bash

sudo apt-get install ros-indigo-motoman*

cd motoman_ws
catkin_make
source /opt/ros/indigo/setup.bash
source ~/app/motoman_ws/devel/setup.bash

sudo apt-get install ros-indigo-motoman*

cd motoman_ws
catkin_make
source /opt/ros/indigo/setup.bash
source ~/app/motoman_ws/devel/setup.bash

echo "export ROS_HOSTNAME=localhost" >> ~/.bashrc && \
echo "export ROS_MASTER_URI=http://localhost:11311" >> ~/.bashrc
source ~/.bashrc